package server;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import data.*;

public class PresenceServerWriter implements Runnable {
	/* Attributes */
	/**//* Received Messages Buffer */
	private List<Message> msgBuffer =  null;
	private MulticastSocket multiSock = null;
	private InetAddress multiGroup = null;
	private int multiPort = 0;
	private WRStatusEnum status = WRStatusEnum.UNDEFINED;
	
	/* Constructors */
	public PresenceServerWriter (MulticastSocket socket, InetAddress group, int multicastPort) {
		this.msgBuffer = Collections.synchronizedList(new ArrayList<Message>());
		this.multiSock = socket;
		this.multiGroup = group;
		this.multiPort = multicastPort;
		this.status = WRStatusEnum.IDLE;
	}
	
	/* Methods */
	/**//* Buffer related methods */
	public boolean hasMessage() {
		return !this.msgBuffer.isEmpty();
	}
	
	private Message nextMessage() throws IOException {
		if (this.hasMessage()) {
			return this.msgBuffer.remove(0);
		} else {
			throw new IOException("The writer's buffer is empty.");
		}
	}
	
	private String getMessageIP() throws IOException {
		if (this.hasMessage()) {
			return (this.msgBuffer.get(0)).getReceiver().getIP();
		} else {
			throw new IOException("The writer's buffer is empty.");
		}
	}
	
	public void addMessage(Message msg) {
		this.msgBuffer.add(msg);
	}
	
	/**//* Status related methods */
	public boolean isRunning() {
		return (this.status==WRStatusEnum.RUNNING);
	}
	
	public boolean isPaused() {
		return (this.status==WRStatusEnum.PAUSED);
	}
	
	public boolean isFinishing() {
		return (this.status==WRStatusEnum.FINISHING);
	}
	
	public boolean isFinished() {
		return (this.status==WRStatusEnum.FINISHED);
	}
	
	public void pause() {
		this.status = WRStatusEnum.PAUSED; 
	}
	
	public void resume() {
		this.status = WRStatusEnum.RUNNING; 
	}
	
	public void finish() {
		this.status = WRStatusEnum.FINISHING; 
	}
	
	public void end() {
		this.status = WRStatusEnum.FINISHED; 
	}
	
	/**//* Thread related methods */
	/**//**//* The run methods */
	@Override
	public void run() {
		//Update the status of this runnable
		this.status = WRStatusEnum.RUNNING;
		//Iterate until finished
		while (!this.isFinished()) {
			
			//The thread stays here if paused
			
			while (this.isRunning()) {
				try {
					if (this.hasMessage()) {
						String IP = this.getMessageIP();
						ByteArrayOutputStream outputByte = new ByteArrayOutputStream();
						ObjectOutputStream outputStream = new ObjectOutputStream(outputByte);
						outputStream.writeObject(this.nextMessage());
						byte[] data = outputByte.toByteArray();
						if (IP.equals("0.0.0.0")) {
							this.multiSock.send(new DatagramPacket(data, data.length, this.multiGroup, this.multiPort));
						}
						else {
							InetAddress ReceiverIP = InetAddress.getByName(IP);
							this.multiSock.send(new DatagramPacket(data, data.length, ReceiverIP, this.multiPort));
						}
					}
				} catch (IOException e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
				}
			}
		}
		
		//End the channel properly
		this.multiSock.close();
		this.end();
	}
	
	
}
