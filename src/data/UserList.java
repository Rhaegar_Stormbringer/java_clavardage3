package data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;

public class UserList extends Observable {
	
	/* Attributes */
	private List<User> list;
	
	/* Constructors */
	public UserList() {
		this.list = Collections.synchronizedList(new ArrayList<User>());
	}
	
	/* Methods */
	/**//* add */
	public void addUser(User u) {
		this.list.add(u);
		this.hasChanged();
		this.notifyObservers();
	}
	
	/**//*remove */
	public void removeUser(User u) {
		System.out.println("[UserList] "+u.getNickname()+" | size "+this.list.size());
		for (int i=0; i < this.list.size(); i++) {
			User b = this.list.get(i);
			if (b.getNickname().equals(u.getNickname())) {
				System.out.println("[UserList] "+b.getNickname()+" est bien dans la liste");
				System.out.println("[UserList] "+this.list.remove(b));
			}
		}
		//this.list.remove(u);
		System.out.println("[UserList] size "+this.list.size());
		this.hasChanged();
		this.notifyObservers();
	}
	
	public List<User> getList() {
		return this.list;
	}
	
	/**//* Specific getter */
	public User getUserByAddress(String ip) {
		Iterator<User> i = this.list.iterator();
		User u = null;
		while (i.hasNext()) {
			u = i.next();
			if (u.getIP().equals(ip)) {
				return u;
			}
		}
		return null;
				
	}
	
	public User getUserByLogin(String login) {
		Iterator<User> i = this.list.iterator();
		User u = null;
		while (i.hasNext()) {
			u = i.next();
			if (u.getLogin().equalsIgnoreCase(login)) {
				return u;
			}
		}
		return null;				
	}
	
	public User getUserByNickname(String nickname) {
		Iterator<User> i = this.list.iterator();
		User u = null;
		while (i.hasNext()) {
			u = i.next();
			if (u.getNickname().equalsIgnoreCase(nickname)) {
				return u;
			}
		}
		return null;				
	}
	
	public ArrayList<User> getOnlineUsersList() {
		Iterator<User> i = this.list.iterator();
		User u = null;
		ArrayList<User> l = new ArrayList<User>();
		while (i.hasNext()) {
			u = i.next();
			if (u.isOnline()) {
				l.add(u);
			}
		}
		return l;
	}
	
	public List<User> getAlphabeticalList() {
		List<User> uL = this.list;
		Collections.sort(uL);
		return uL;
	}
}
