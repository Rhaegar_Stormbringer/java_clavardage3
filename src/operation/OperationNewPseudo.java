package operation;

import app.AppModel;
import data.Message;
import data.User;
import data.UserStatusEnum;

public class OperationNewPseudo extends Operation {
	/* New Attributes */
	private String login;
	private String nickname;
	private AppModel appModel;
	
	/* Constructors */
	public OperationNewPseudo(AppModel am) {
		this.appModel = am;
		this.execute();
	}
	
	/* Define Abstract Methods */
	@Override
	public void execute() {
		System.out.println("[USER] : "+this.appModel.getLocalUser());
		
		/* Send info to multicast */
		this.appModel.getMultiChannel().sendMessage(Message.multicastNewPseudoMessage(this.appModel.getLocalUser()));
		System.out.println("message envoyé pour changement de pseudo");

		/* Waiting for userList */
		/*
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		/* Check userList */
		/*
		if (this.appModel.getUserList().getUserByLogin(this.login) != null) {
			//Fail
			this.fail();
			this.setInfo("Pseudo already taken");
		} else if (this.appModel.getUserList().getUserByNickname(this.nickname) != null) {
			//Fail
			this.fail();
			this.setInfo("Pseudo already taken");
		} else {
			//Success
			this.success();
			this.setInfo("success");
		}
		*/
	}
}
