package app;


import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

import com.*;
import data.*;

public class Main {
	public static void main(String[] args) throws SocketException, InterruptedException {
		
		
		/* Variables */
		AppModel thisModel = null;
		String remoteIP = null;
		int remotePort = -1;
		//ClientChannel remoteChannel = null;
		
		/* Execution */
		try {
			//Retrieve IP
			DatagramSocket ipSock = new DatagramSocket();
			ipSock.connect(InetAddress.getByName("8.8.8.8"), 0);
			String ip = ipSock.getLocalAddress().getHostAddress();
			ipSock.close();
			
			//Create the Model
			thisModel = new AppModel("1234", "monika", ip,0);
			
			//Print your address
			System.out.println(thisModel.getServer().getLocalIP() + " | " +thisModel.getServer().getLocalPort());
			
			/* ---- MULTICASTING (CONNEXION) ---- */
			
			Thread.sleep(100);
			System.out.println("connecting to network ...");
			Message MSG_CONNEXION = Message.multicastConnectionMessage(thisModel.getLocalUser());
			thisModel.getMultiChannel().sendMessage(MSG_CONNEXION);
			Thread.sleep(4000);
			System.out.println("end of connection phase");
			System.out.println("users connected : ");
			for (User u : thisModel.getUserList().getOnlineUsersList()) {
				System.out.println(u.toString());
			}
			
			/* ---------------------------------- */
			
			//Request interlocutor address
			Scanner scan = new Scanner(System.in);
			System.out.print("Enter your interlocutor IP address : ");
			remoteIP = scan.nextLine();
			System.out.print("Enter your interlocutor port : ");
			remotePort = Integer.parseInt(scan.nextLine());
			scan.close();
			
			//Establish connection :
			ClientChannel cc= new ClientChannel(remoteIP,remotePort);
			thisModel.addChannel(cc);
			new Thread(cc).start();
			//remoteChannel = thisModel.getChannel(remoteIP, remotePort);
			
			//Send messages :
			/*for(int i = 0 ; i <10 ; i++) {
				remoteChannel.sendMessage(new Message("A","B","Message #"+i));
				System.out.println("Message send");
			}*/
		
		} catch (UnknownHostException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}