package server;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import data.*;

public class PresenceServerReader implements Runnable {
	
	/* Attributes */
	/**//* Received Messages Buffer */
	private List<Message> msgBuffer =  null;
	private MulticastSocket multiSock = null;
	private InetAddress multiGroup = null;
	private int multiPort = 0;
	private final int bufferSize = 1024*4; //Maximum size of transfer object
	private WRStatusEnum status = WRStatusEnum.UNDEFINED;
	
	/* Constructors */
	public PresenceServerReader (MulticastSocket socket, InetAddress group, int multicastPort) {
		this.msgBuffer = Collections.synchronizedList(new ArrayList<Message>());
		this.multiSock = socket;
		this.multiGroup = group;
		this.multiPort = multicastPort;
		this.status = WRStatusEnum.IDLE;
	}
	
	/* Methods */
	/**//* Buffer related methods */
	public boolean hasMessage() {
		return !this.msgBuffer.isEmpty();
	}
	
	public Message nextMessage() throws IOException {
		if (this.hasMessage()) {
			return this.msgBuffer.remove(0);
		} else {
			throw new IOException("The reader's buffer is empty.");
		}
	}
	
	private void addMessage(Message msg) {
		this.msgBuffer.add(msg);
	}
	
	/**//* Status related methods */
	public boolean isRunning() {
		return (this.status==WRStatusEnum.RUNNING);
	}
	
	public boolean isPaused() {
		return (this.status==WRStatusEnum.PAUSED);
	}
	
	public boolean isFinishing() {
		return (this.status==WRStatusEnum.FINISHING);
	}
	
	public boolean isFinished() {
		return (this.status==WRStatusEnum.FINISHED);
	}
	
	public void pause() {
		this.status = WRStatusEnum.PAUSED; 
	}
	
	public void resume() {
		this.status = WRStatusEnum.RUNNING; 
	}
	
	public void finish() {
		this.status = WRStatusEnum.FINISHING; 
	}
	
	public void end() {
		this.status = WRStatusEnum.FINISHED; 
	}
	
	
	/**//* Thread related methods */
	/**//**//* The run methods */
	@Override
	public void run() {
		//Update the status of this runnable
		this.status = WRStatusEnum.RUNNING;
		//Iterate until finished
		while (!this.isFinished() && !this.isFinished()) {
			
			//The thread stays here if paused            			
			while (this.isRunning()) {
				try {
					byte[] buffer = new byte[bufferSize];
					DatagramPacket messageReceived = new DatagramPacket(buffer, bufferSize, this.multiGroup, this.multiPort);
					/*System.err.println("waiting for message ...");*/
					this.multiSock.receive(messageReceived);
		            /*System.out.println("Datagram multicast received!");*/
		            try {
		            	ByteArrayInputStream bais = new ByteArrayInputStream(messageReceived.getData());
						ObjectInputStream ois =  new ObjectInputStream(bais);
		                Object readObject = ois.readObject();
		                if (readObject instanceof Message) {
		                    Message message = (Message) readObject;
		                    /*System.out.println("Message is: " + message);
		                    System.out.println("flag CONNECTION : "+message.getFlag(FlagEnum.USER_CONNECTION));
		                    System.out.println("flag CONNECTED : "+message.getFlag(FlagEnum.USER_CONNECTED));*/
		                    this.addMessage(message);
		                } /*else {
		                    System.out.println("The received object is not of type Message !");
		                }*/
		            ois.close();
		            bais.close();
		            messageReceived = null;
		            } catch (Exception e) {
		                System.out.println("No object could be read from the received UDP datagram.");
		            }
				} catch (IOException e) {
					System.out.println("Unable to retrieve data from the input stream.");
					e.printStackTrace();
				}
			}
		}
		//End properly the channel
		this.multiSock.close();
		this.end();
	}
}