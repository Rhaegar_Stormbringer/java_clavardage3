package com;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import data.*;

public class ClientChannel extends Observable implements Runnable {

	/* Attributes */
	/**//* Main attributes */
	private Socket socket = null;
	//private User distantUser  = null; //Unused so far 
	
	/**//* Linked threads */ 
	private ClientReader reader = null;
	private ClientWriter writer = null;
	
	/**//* Buffers */
	private List<Message> outMessages = null;
	private List<Message> allMessages = null;  
	
	/**//* Status */
	private ChannelStatusEnum status = ChannelStatusEnum.UNDEFINED;
  
	/* Constructors */
	/**//* Constructor by Address and Port*/
	public ClientChannel(String distantHost, int distantPort/*, String login*/) throws UnknownHostException, IOException { //Unused so far 
		//this.login = login; //Unused so far 
		this.outMessages = Collections.synchronizedList(new ArrayList<Message>());
		this.allMessages = Collections.synchronizedList(new ArrayList<Message>());
		this.status = ChannelStatusEnum.IDLE;
		this.socket = new Socket(distantHost, distantPort);
		this.writer = new ClientWriter(new ObjectOutputStream(this.socket.getOutputStream()));
		this.reader = new ClientReader(new ObjectInputStream(this.socket.getInputStream()));	
	} 
	
	
	/**//* Constructor by Socket */
	public ClientChannel(Socket socket/*, String login*/)  { //Unused so far 
		//this.login = login; //Unused so far 
		this.outMessages = Collections.synchronizedList(new ArrayList<Message>());
		this.allMessages = Collections.synchronizedList(new ArrayList<Message>());
		this.status = ChannelStatusEnum.IDLE;
		
		try {
			this.socket = socket;
			this.writer = new ClientWriter(new ObjectOutputStream(this.socket.getOutputStream()));
			this.reader = new ClientReader(new ObjectInputStream(this.socket.getInputStream()));
		}
		catch (UnknownHostException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}  

	/* Methods */
	/**//* Socket related methods */
	public int getPort() {
		return this.socket.getLocalPort();
	}
	
	public String getIP() {
		return this.socket.getInetAddress().getHostAddress();
	}
	/**//* Status related methods */
	public boolean isRunning() {
		return (this.status==ChannelStatusEnum.RUNNING);
	}
	
	public boolean isPaused() {
		return (this.status==ChannelStatusEnum.PAUSED);
	}
	
	public boolean isFinishing() {
		return (this.status==ChannelStatusEnum.FINISHING);
	}
	
	public boolean isFinished() {
		return (this.status==ChannelStatusEnum.FINISHED);
	}
	
	public void pause() {
		this.status = ChannelStatusEnum.PAUSED; 
	}
	
	public void resume() {
		this.status = ChannelStatusEnum.RUNNING; 
	}
	
	public void finish() {
		this.status = ChannelStatusEnum.FINISHING; 
	}
	
	public void end() {
		this.status = ChannelStatusEnum.FINISHED; 
	}
	
	
	/**//* Buffer related methods */
	public void sendMessage(Message msg) {
		if (!this.isFinishing() && !this.isFinished()) {
			this.outMessages.add(msg);
		}
	}
	
	public boolean hasMessageInOutbox() {
		return (!this.outMessages.isEmpty());
	}
	
	private Message nextMessage() throws IOException{
		if (this.hasMessageInOutbox()) {
			return this.outMessages.remove(0);
		} else {
			throw new IOException("There's no message in the outbox.");
		}		
	}
	
	public List<Message> getAllMessageList() {
		return this.allMessages;
	}
	
	public void setAllMessageList(List<Message> lm) {
		this.allMessages = new ArrayList<Message>(lm);
	}
	
	public String getRemoteIP() {
		return this.socket.getInetAddress().getHostAddress();
	}
	
	public int getRemotePort() {
		return this.socket.getPort();
	}
	
	private void addMessage(Message msg) {
		this.allMessages.add(msg);
		this.setChanged();
		this.notifyObservers(this.getRemoteIP());
	}
	
	public void addSystemMessage(Message msg) {
		if (msg.getSender().getLogin().equals("System") && msg.getReceiver().getLogin().equals("LocalUser")) {
			this.addMessage(msg);
		}
	}
	
	private void retrieveMessage() throws IOException {
		//Current processed message
		Message currentMsg   = null;
		Flags   currentFlags = null;
		
		currentMsg   = this.reader.nextMessage();
		currentFlags = currentMsg.getFlags();
		//Flag processing
		if (currentFlags.areActivated()) {
			if (currentFlags.getFlag(FlagEnum.END_OF_CONVERSATION)) {
				//Go to the "Finish properly"
				this.addMessage(Message.systemMessage("REMOTE USER IS DISCONNECTED."));
				this.finish();
			} //Insert other case(s) here if required
		} else {
			//Default Behaviour
			this.addMessage(currentMsg);
			System.out.println(currentMsg);
		}
	}
	
	/**//* Thread related methods */
	/**//**//* The run method */
	@Override
	public void run() {
		//Update the status of this Runnable
		this.status = ChannelStatusEnum.RUNNING;
		//Start both the Reader and the Writer in threads
		new Thread(this.reader).start();
		new Thread(this.writer).start();

		
		//Iterate until the socket is closed
		while (!this.socket.isClosed() && !this.isFinished() && !this.isFinishing()) {
			
			//The thread stays here while paused
			
			while (this.isRunning()) {
				try {
					//Send a message
					if (this.hasMessageInOutbox()) {
						Message nM = this.nextMessage();
						this.writer.addMessage(nM);
						if(!nM.getContent().equals("")) {
							this.addMessage(nM);
						}
					}
					//Receive a message and process it (flag, etc)
					if (this.reader.hasMessage()) {
						this.retrieveMessage();
						
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}		
		//Finish properly the channel
		this.writer.finish();
		this.reader.finish();
		
		while(!this.writer.isFinished()&&!this.writer.isFinished()) {
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				break;
			}
		}
		
		try {
			this.socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		this.end();
	}
}