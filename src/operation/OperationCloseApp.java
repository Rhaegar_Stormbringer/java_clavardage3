package operation;

import app.AppModel;

public class OperationCloseApp extends Operation {
	/* Attributes */
	private AppModel appModel;
	
	/* Constructors */
	public OperationCloseApp(AppModel am) {
		this.appModel = am;
		this.execute();
	}
	
	/* Methods */
	@Override
	public void execute() {
		Operation oLogout = new OperationLogout(this.appModel);
		System.out.println(oLogout.getInfo());
		this.appModel.getServer().finish();
		if (oLogout.isSuccessful()) {
			this.success();
			this.setInfo("You may now close the app.");
		} else {
			this.fail();
			this.setInfo("You either fail to logout or to close the server properly.");
		}
		
	}

}
