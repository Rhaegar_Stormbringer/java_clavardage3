package data;

public enum WRStatusEnum {
	UNDEFINED,
	IDLE,
	RUNNING,
	PAUSED,
	FINISHING,
	FINISHED;
}
