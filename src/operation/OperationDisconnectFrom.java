package operation;

import app.AppModel;
import data.FlagEnum;
import data.Message;
import data.User;

public class OperationDisconnectFrom extends Operation {
	/* Attributes */
	private User remoteUser;
	private AppModel appModel;
	
	/* Constructors */
	public OperationDisconnectFrom(User u, AppModel appModel) {
		this.remoteUser = u;
		this.appModel = appModel;
		this.execute();
	}

	/* Methods */
	@Override
	public void execute() {
		//Send an END_OF_CONVERSATION message
		Message endMsg = new Message(this.appModel.getLocalUser(), this.remoteUser, "");
		endMsg.raiseFlag(FlagEnum.END_OF_CONVERSATION);
		this.appModel.getChannel(this.remoteUser.getIP()).sendMessage(endMsg);
		
		//Create a local system message
		Message systEndMsg = Message.systemMessage("You have ended this conversation.");
		this.appModel.getChannel(this.remoteUser.getIP()).addSystemMessage(systEndMsg);
		
		//Close the channel
		this.appModel.getChannel(this.remoteUser.getIP()).finish();
		
		//Success
		this.success();
		this.setInfo("You have successfully ended this conversation.");
	}

}
