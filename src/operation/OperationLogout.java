package operation;

import java.util.Iterator;

import app.AppModel;
import data.Message;
import data.User;
import data.UserStatusEnum;

public class OperationLogout extends Operation {

	/* Attributes */
	private AppModel appModel;
	
	/* Constructors */
	public OperationLogout(AppModel am) {
		this.appModel = am;
		this.execute();
	}

	/* Override abstract methods */
	@Override
	public void execute() {
		//Notify everyone
		System.out.println("OUI");
		this.appModel.getMultiChannel().sendMessage(Message.multicastDisconnectionMessage(this.appModel.getLocalUser()));
		
		//Close all the channel :
		Iterator<User> i = this.appModel.getUserList().getList().iterator();
		User u = null;
		while(i.hasNext()) {
			u = i.next();
			if (this.appModel.getChannel(u.getIP()) != null) {
				this.appModel.getChannel(u.getIP()).finish();
			}			
		}
		
		//Suppress the local user
		User localUser = this.appModel.getLocalUser();
		User newLocalUser = new User(null, null, localUser.getIP(), localUser.getPort(), UserStatusEnum.CONNECTED);
		this.appModel.setLocalUser(newLocalUser);
		
		//Success
		this.success();
		this.setInfo("You are logged out");
	}
}
