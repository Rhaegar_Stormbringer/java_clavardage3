package controller;

import java.net.URL;
import java.util.Iterator;
import java.util.Observable;
import java.util.ResourceBundle;

import com.ClientChannel;

import app.AppController;
import app.AppModel;
import data.Message;
import data.User;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import operation.Operation;
import operation.OperationConnectTo;
import operation.OperationDisconnectFrom;
import operation.OperationLogin;
import operation.OperationLogout;
import operation.OperationNewPseudo;
import operation.OperationSend;
import operation.OperationWantPseudo;

// imports...

public class ChatController extends AppController implements Initializable {
	
	@FXML private ListView<String> passiveUsers;
    @FXML private ListView<String> activeUsers;
    @FXML private TextField chatTextField;
    @FXML private TextArea chatTextArea;
    
    @FXML private Text pseudoText;
    @FXML private TextField pseudoTextField;
    @FXML private Button pseudoButton;
    
	private AppModel appModel;
   
	/* Methods */    
    public void createModel(AppModel appModel) {
    	this.appModel = appModel;
    	this.appModel.addObserver(this);
    	this.appModel.setAppController(this);
		this.refreshPassiveUsers();
		this.refreshActiveUsers();
    }
    
	/**//* Do methods */
	public Operation doLogin(String login, String nickname) {
		System.out.println(login);
		return new OperationLogin(login, nickname, this.appModel);		
	}
	
	public Operation doLogout() {
		return new OperationLogout(this.appModel);
	}
	
	public Operation doConnectTo(User u) {
		return new OperationConnectTo(u, this.appModel, this);
	}
	
	public Operation doDisconnectFrom(User u) {
		return new OperationDisconnectFrom(u, this.appModel); //TO-DO
	}
	
	public Operation doSend(Message m) {
		return new OperationSend(m, this.appModel); //TO-DO
	}
	
	public Operation doWantPseudo(String newPseudo) {
		return new OperationWantPseudo(newPseudo, this.appModel);
	}
	
	public Operation doNewPseudo() {
		return new OperationNewPseudo(this.appModel);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.passiveUsers.setCellFactory(param -> new ListCell<String>() {
		    @Override
		    protected void updateItem(String item, boolean empty) {
		        super.updateItem(item, empty);
		        if (empty || item == null) {
		            setText(null);
		        } else {
		            setText(item);
		        }
		    }
		});
		
		this.passiveUsers.setOnMouseClicked(new EventHandler<MouseEvent>() {

	        @Override
	        public void handle(MouseEvent event) {
	        	if (!passiveUsers.getSelectionModel().getSelectedItem().isEmpty()) {
	        		System.out.println("clicked on "+passiveUsers.getSelectionModel().getSelectedItem());
	        		User u = appModel.getUserList().getUserByNickname(passiveUsers.getSelectionModel().getSelectedItem());
	        		ClientChannel c = appModel.getChannel(u.getIP());
	        		if (c == null || c.isFinished() || c.isFinishing()) {
		        		System.out.println(doConnectTo(u).getInfo());	
	        		}
	        		Platform.runLater(new Runnable() {

	        			@Override
	        				public void run() {
	        				refreshTextArea(c);
	        				}
	        	    });
	        		
	        	}
	        	Platform.runLater(new Runnable() {

        			@Override
        				public void run() {
        	        	refreshActiveUsers();
        				}
        	    });
     	        event.consume();
	        }
	    });
		
		this.activeUsers.setCellFactory(e -> new ListCell<String>() {
			@Override
		    protected void updateItem(String item, boolean empty) {
		        super.updateItem(item, empty);
		        if (empty || item == null) {
		            setText(null);
		        } else {
		            setText(item);
		        }
		    }
		});
		
		this.activeUsers.setOnMouseClicked(new EventHandler<MouseEvent>() {

	        @Override
	        public void handle(MouseEvent event) {
	        	if (!activeUsers.getSelectionModel().getSelectedItem().isEmpty()) {
	        		System.out.println("clicked on "+activeUsers.getSelectionModel().getSelectedItem());
	        		User u = appModel.getUserList().getUserByNickname(activeUsers.getSelectionModel().getSelectedItem());
	        		System.out.println(doDisconnectFrom(u));
	        		//refreshTextArea(appModel.getChannel(u.getIP()));
	        	}
	        	Platform.runLater(new Runnable() {

        			@Override
        				public void run() {
        	        	refreshActiveUsers();
        				}
        	      	});
     	        event.consume();
	        }
	    });
		
		this.chatTextField.setOnAction(new EventHandler<ActionEvent>()
	    {
			@Override
			public void handle(ActionEvent event) {
				if (chatTextField != null && !chatTextField.getText().trim().isEmpty()) {
					System.out.println("focus on "+passiveUsers.getSelectionModel().getSelectedItem());
	        		User u = appModel.getUserList().getUserByNickname(passiveUsers.getSelectionModel().getSelectedItem());
	        		ClientChannel c = appModel.getChannel(u.getIP());
	        		if (c == null || c.isFinished() || c.isFinishing()) {
		        		System.out.println(doConnectTo(u).getInfo());
	        		}
	        		Message m = new Message(appModel.getLocalUser(), u, chatTextField.getText());
	        		doSend(m);
	        		Platform.runLater(new Runnable() {

	        			@Override
	        				public void run() {
	    	        		refreshTextArea(c);
	    					chatTextField.setText("");
	        				}
	        	      	});
	        		}
			}
		});
		
		this.chatTextArea.setEditable(false);
		this.chatTextArea.setWrapText(true);
		
		this.pseudoText.setText("");
		this.pseudoTextField.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if(doWantPseudo(pseudoTextField.getText()).getInfo().equals("success")) {
					System.out.println("pseudo choisi bon !");
					doNewPseudo();
				}
				else {
					pseudoText.setText("Ce pseudo est deja pris");
				}
				pseudoTextField.setText("");
			}});
	}
	
	public void refreshPassiveUsers() {
		int j = 0;
		this.passiveUsers.getItems().clear();
		System.out.println("size : "+this.appModel.getUserList().getList().size());
		for (User u : this.appModel.getUserList().getList()) {
			System.out.println(u+" | "+u.getNickname());
		}
		Iterator<User> i = this.appModel.getUserList().getList().iterator();
		while(i.hasNext()) {
			this.passiveUsers.getItems().add(i.next().getNickname());
			j ++;
		}
		while (j < 24) {
			this.passiveUsers.getItems().add("");
			j ++;
		}
		this.passiveUsers.requestFocus();
		this.refreshActiveUsers();
	}
	
	public void refreshActiveUsers() {
		int j = 0;
		this.activeUsers.getItems().clear();
		Iterator<User> i = this.appModel.getUserList().getList().iterator();
		while(i.hasNext()) {
			User u = i.next();
			if (this.appModel.getChannel(u.getIP()) != null && !this.appModel.getChannel(u.getIP()).isFinished() && !this.appModel.getChannel(u.getIP()).isFinishing()) {
				this.activeUsers.getItems().add(u.getNickname());
				j ++;
			}			
		}
		while (j < 20) {
			this.activeUsers.getItems().add("");
			j ++;
		}
		this.activeUsers.requestFocus();
	}
	
	public void refreshTextArea(ClientChannel cc) {
		User u = this.appModel.getUserList().getUserByAddress(cc.getRemoteIP());
		System.err.println(u.getNickname());
		if (this.passiveUsers.getSelectionModel().getSelectedItem() == u.getNickname()) {
			String content = "";
			Iterator<Message> m = cc.getAllMessageList().iterator();
			while(m.hasNext()) {
				content += m.next()+"\n";
				System.err.println(content);
			}
			this.chatTextArea.setText(content);	
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o == this.appModel) {
			System.out.println("user enleve ou rajoute dans userList");
	      	Platform.runLater(new Runnable() {

			@Override
				public void run() {
				refreshPassiveUsers();
				}
	      	});
	    }
		else if (o instanceof ClientChannel) {
			Platform.runLater(new Runnable() {

				@Override
					public void run() {
					refreshActiveUsers();
					refreshTextArea((ClientChannel) o);
					}
		    });
		}
	}
}