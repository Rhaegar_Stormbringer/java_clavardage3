package server;



import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import app.AppModel;
import data.*;

public class PresenceServerChannel implements Runnable {
	
	/* Attributes */
	/**//* Main attributes */
	private MulticastSocket socket = null;
	private UserList userList;
	private User user;
	
	//private String login  = "Client"; //Unused so far 
	
	/**//* Linked threads */ 
	private PresenceServerReader reader = null;
	private PresenceServerWriter writer = null;
	
	/**//* Buffers */
	private List<Message> outMessages = null;
	private List<Message> allMessages = null;
	
	/*private final int bufferSize = 1024*4; */
	
	/**//* Status */
	private ChannelStatusEnum status = ChannelStatusEnum.UNDEFINED;
  
	/* Constructors */
	/**//* Constructor by Address and Port*/
	public PresenceServerChannel(int distantPort) { 
		//this.login = login; //Unused so far
		this.userList = new UserList();
		this.outMessages = Collections.synchronizedList(new ArrayList<Message>());
		this.allMessages = Collections.synchronizedList(new ArrayList<Message>());
		this.status = ChannelStatusEnum.IDLE;
		
		try {
			this.socket = new MulticastSocket(distantPort); // FIXE !!!
			InetAddress group = InetAddress.getByName("225.0.0.0");
			this.socket.joinGroup(group);
			this.writer = new PresenceServerWriter(this.socket, group, distantPort);
			this.reader = new PresenceServerReader(this.socket, group, distantPort);
			this.user  = new User("SERVER", "SERVER", this.socket.getInetAddress().getHostAddress(), distantPort);
		}
		catch (UnknownHostException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	/* Methods */
	/**//* Socket related methods */
	public int getPort() {
		return this.socket.getPort();
	}
	
	public String getIP() {
		return this.socket.getInetAddress().getHostAddress();
	}
	/**//* Status related methods */
	public boolean isRunning() {
		return (this.status==ChannelStatusEnum.RUNNING);
	}
	
	public boolean isPaused() {
		return (this.status==ChannelStatusEnum.PAUSED);
	}
	
	public boolean isFinishing() {
		return (this.status==ChannelStatusEnum.FINISHING);
	}
	
	public boolean isFinished() {
		return (this.status==ChannelStatusEnum.FINISHED);
	}
	
	public void pause() {
		this.status = ChannelStatusEnum.PAUSED; 
	}
	
	public void resume() {
		this.status = ChannelStatusEnum.RUNNING; 
	}
	
	public void finish() {
		this.status = ChannelStatusEnum.FINISHING; 
	}
	
	public void end() {
		this.status = ChannelStatusEnum.FINISHED; 
	}
	
	@Override
	public String toString() {
		String s = "|| ---------------- [MAJ LIST USERS] ---------------- ||\n";
		for (User u : this.userList.getList()) {
			s += u.getNickname()+" | ";		
		}
		s += "\n|| -------------------------------------------------- ||\n|| Total : "+this.userList.getList().size()+" 										  ||\n";
		return s;
	}
	
	/**//* Buffer related methods */
	public void sendMessage(Message msg) {
		this.outMessages.add(msg);
	}
	
	public boolean hasMessageInOutbox() {
		return (!this.outMessages.isEmpty());
	}
	
	private Message nextMessage() throws IOException{
		if (this.hasMessageInOutbox()) {
			return this.outMessages.remove(0);
		} else {
			throw new IOException("There's no message in the outbox.");
		}		
	}
	
	@SuppressWarnings("unused")
	private void addMessage(Message msg) {
		this.allMessages.add(msg);
	}
	
	/**//* Thread related methods */
	
	
	/**//**//* The start method */
	public void start() {
		new Thread(this).start();
	}
	
	/**//**//* The run method */
	@Override
	public void run() {
		//Update the status of this Runnable
		this.status = ChannelStatusEnum.RUNNING;
		//Start both the Reader and the Writer in threads
		new Thread(this.reader).start();
		new Thread(this.writer).start();
		//Current processed message
		Message currentMsg   = null;
		Flags   currentFlags = null;
		
		//Iterate until the socket is closed
		while (!this.socket.isClosed() && !this.isFinished() && !this.isFinishing()) {
			
			//The thread stays here while paused
			
			while (this.isRunning()) {
				try {
					//Send a message
					if (this.hasMessageInOutbox()) {
						this.writer.addMessage(this.nextMessage());
					}
					//Receive a message and process it (flag, etc)
					if (this.reader.hasMessage()) {
						currentMsg   = this.reader.nextMessage();
						currentFlags = currentMsg.getFlags();
						//this.addMessage(currentMsg); 	// useless in multicasting
						
						//Flag processing
						if (currentFlags.areActivated()) {
							if (currentFlags.getFlag(FlagEnum.SERVER_STATUS)) {
									this.sendMessage(Message.unicastServerStatusMessage(this.user, currentMsg.getSender()));	
								}
							}
						
							if (currentFlags.getFlag(FlagEnum.SERVER_CONNECTED)) {
								this.userList.addUser(currentMsg.getSender());
								System.out.println(this.toString());
								this.sendMessage(Message.unicastServerStatusMessage(this.user, currentMsg.getSender()));	
							}
						}
					}
				catch (IOException e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
				}
			}
		}
		//End the channel properly
		this.socket.close();
		this.reader.finish();
		this.writer.finish();
		this.end();
	}
}