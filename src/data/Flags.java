package data;

import java.io.Serializable;

public class Flags implements Serializable{
	/**
	 * Automatically generated serialVersionUID
	 */
	private static final long serialVersionUID = -2349613925143736391L;
	
	
	/* Attributes */	
	private boolean activated         = false;
	private boolean endOfConversation = false;
	private boolean userConnection   = false;
	private boolean userDisconnection = false;
	private boolean userConnected = false;
	private boolean userWantPseudo = false;
	private boolean userNewPseudo = false;
	private boolean multicast = false;
	private boolean system    = false;
	
	/* Constructors */
	public Flags() {
		/* Do nothing */
	}
	
	/* Methods */
	public boolean haveRaisedFlag() {
		return (this.endOfConversation || this.userConnection || this.userConnected || this.multicast || this.system || this.userDisconnection);
	}
	
	public boolean areActivated() {
		return this.activated;
	}	
	
	public void raiseFlag(FlagEnum flag) {
		switch (flag) {
		case END_OF_CONVERSATION:
			this.activated = true;
			this.endOfConversation = true;
			break;
		case USER_CONNECTION:
			this.activated = true;
			this.userConnection = true;
			break;
		case USER_DISCONNECTION:
			this.activated = true;
			this.userDisconnection = true;
			break;
		case USER_CONNECTED:
			this.activated = true;
			this.userConnected = true;
			break;
		case USER_WANT_PSEUDO:
			this.activated = true;
			this.userWantPseudo = true;
			break;
		case USER_NEW_PSEUDO:
			this.activated = true;
			this.userNewPseudo = true;
			break;
		case MULTICAST:
			this.activated = true;
			this.multicast = true;
			break;
		case SYSTEM:
			this.activated = true;
			this.system = true;
			break;
		default:
			break;
		
		}
	}
	
	public void lowerFlag(FlagEnum flag) {
		switch (flag) {
		case END_OF_CONVERSATION:
			this.endOfConversation = false;
			break;
		case USER_CONNECTION:
			this.userConnection = false;
			break;
		case USER_CONNECTED:
			this.userConnected = false;
			break;
		case USER_WANT_PSEUDO:
			this.userWantPseudo = false;
			break;
		case USER_NEW_PSEUDO:
			this.userNewPseudo = false;
			break;
		case USER_DISCONNECTION:
			this.userDisconnection = false;
			break;
		case MULTICAST:
			this.multicast = false;
		case SYSTEM:
			this.system = false;
		default:
			break;		
		}
		if (!this.haveRaisedFlag()) {
			this.activated = false;
		}
	}
	
	public boolean getFlag(FlagEnum flag) {
		switch (flag) {
		case END_OF_CONVERSATION:
			return this.endOfConversation;
		case USER_CONNECTION:
			return this.userConnection;
		case USER_CONNECTED:
			return this.userConnected;
		case USER_WANT_PSEUDO:
			return this.userWantPseudo;
		case USER_NEW_PSEUDO:
			return this.userNewPseudo;
		case MULTICAST:
			return this.multicast;
		case SYSTEM:
			return this.system;
		case USER_DISCONNECTION:
			return this.userDisconnection;
		default:
			return false;
		}
	}
	
}
