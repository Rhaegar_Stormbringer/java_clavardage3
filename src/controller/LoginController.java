package controller;

import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.ResourceBundle;

import app.AppController;
import app.AppModel;
import data.User;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import operation.Operation;

public class LoginController extends AppController implements Initializable {

    @FXML private TextField textField ;
    @FXML private Button button;
    @FXML private Text textError;
	
   
	/* Methods */
    /**//* Create methods */    
    public void createModel(AppModel appModel) {
    	this.appModel = appModel;
    	this.appModel.setAppController(this);
    	System.err.println(this.appModel.getServer().getLocalIP() + " | " +this.appModel.getServer().getLocalPort());
    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.textError.setWrappingWidth(150);
		this.button.setOnAction(new EventHandler<ActionEvent>()
	    {
			@Override
			public void handle(ActionEvent event) {
				if (checkPseudo()) {
					changeScene(event);
				}
			}
	    });
		
		this.textField.setOnAction(new EventHandler<ActionEvent>()
	    {
			@Override
			public void handle(ActionEvent event) {
				if (checkPseudo()) {
					changeScene(event);
				}
			}
	    });
	}
	
	public boolean checkPseudo() {
		boolean check = false;
		if (this.textField == null || this.textField.getText().trim().isEmpty()) {
			this.textError.setText("Veuillez entrer un pseudo");
			this.textError.setVisible(true);
		}
		else {
			Iterator<User> i = this.appModel.getUserList().getList().iterator();
			while(i.hasNext()) {
				System.out.println(i.next());
			}
			Operation OP = this.doLogin(this.textField.getText(), this.textField.getText());
			if (OP.isSuccessful()) {
				this.textError.setText(OP.getInfo());
				this.textError.setVisible(true);
				check = true;
			}
			else
			{
				this.textError.setText(OP.getInfo());
				this.textError.setVisible(true);
			}
		}
		this.textField.setText("");
		this.textField.requestFocus();
		return check;
	}
	
	public void changeScene(ActionEvent event) {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader();
			Parent rootChat = fxmlLoader.load(getClass().getResource("../main/chatGUI.fxml").openStream());;
			ChatController fooController = (ChatController) fxmlLoader.getController();
			fooController.createModel(this.appModel);
			Scene sceneChat = new Scene(rootChat);
			Stage mainStage = (Stage) ((Node)event.getSource()).getScene().getWindow();
			
			mainStage.setScene(sceneChat);
			mainStage.centerOnScreen();
			mainStage.show();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
	}
	*/
}