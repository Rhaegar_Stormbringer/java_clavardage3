package com;



import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import app.AppModel;
import data.*;

public class MulticastChannel extends Observable implements Runnable {
	
	/* Attributes */
	/**//* Main attributes */
	private AppModel appModel = null;
	private MulticastSocket socket = null;
	//private String login  = "Client"; //Unused so far 
	
	/**//* Linked threads */ 
	private MulticastReader reader = null;
	private MulticastWriter writer = null;
	
	/**//* Buffers */
	private List<Message> outMessages = null;
	private List<Message> allMessages = null;  
	
	/*private final int bufferSize = 1024*4; */
	
	/**//* Status */
	private ChannelStatusEnum status = ChannelStatusEnum.UNDEFINED;
  
	/* Constructors */
	/**//* Constructor by Address and Port*/
	public MulticastChannel(int distantPort, AppModel appModel) { 
		//this.login = login; //Unused so far
		this.appModel = appModel;
		this.outMessages = Collections.synchronizedList(new ArrayList<Message>());
		this.allMessages = Collections.synchronizedList(new ArrayList<Message>());
		this.status = ChannelStatusEnum.IDLE;
		
		try {
			this.socket = new MulticastSocket(distantPort); // FIXE !!!
			InetAddress group = InetAddress.getByName("230.0.0.0");
			this.socket.joinGroup(group);
			this.writer = new MulticastWriter(this.socket, group, 6666);
			this.reader = new MulticastReader(this.socket, group, 6666);
		}
		catch (UnknownHostException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}  
	
	/**//* Constructor by Socket */
	public MulticastChannel(AppModel appModel, MulticastSocket socket/*, String login*/)  { //Unused so far 
		//this.login = login; //Unused so far 
		this.appModel = appModel;
		this.outMessages = Collections.synchronizedList(new ArrayList<Message>());
		this.allMessages = Collections.synchronizedList(new ArrayList<Message>());
		this.status = ChannelStatusEnum.IDLE;
		
		try {
			this.socket = socket;
			this.socket.joinGroup(this.socket.getInetAddress());;
			InetAddress group = InetAddress.getByName("230.0.0.0");
			this.writer = new MulticastWriter(this.socket, group, 6666);
			this.reader = new MulticastReader(this.socket, group, 6666);
		}
		catch (UnknownHostException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}  

	/* Methods */
	/**//* Socket related methods */
	public int getPort() {
		return this.socket.getPort();
	}
	
	public String getIP() {
		return this.socket.getInetAddress().getHostAddress();
	}
	/**//* Status related methods */
	public boolean isRunning() {
		return (this.status==ChannelStatusEnum.RUNNING);
	}
	
	public boolean isPaused() {
		return (this.status==ChannelStatusEnum.PAUSED);
	}
	
	public boolean isFinishing() {
		return (this.status==ChannelStatusEnum.FINISHING);
	}
	
	public boolean isFinished() {
		return (this.status==ChannelStatusEnum.FINISHED);
	}
	
	public void pause() {
		this.status = ChannelStatusEnum.PAUSED; 
	}
	
	public void resume() {
		this.status = ChannelStatusEnum.RUNNING; 
	}
	
	public void finish() {
		this.status = ChannelStatusEnum.FINISHING; 
	}
	
	public void end() {
		this.status = ChannelStatusEnum.FINISHED; 
	}
	
	
	/**//* Buffer related methods */
	public void sendMessage(Message msg) {
		System.out.println("allo ? "+msg.getFlag(FlagEnum.USER_WANT_PSEUDO));
		this.outMessages.add(msg);
	}
	
	public boolean hasMessageInOutbox() {
		return (!this.outMessages.isEmpty());
	}
	
	private Message nextMessage() throws IOException{
		if (this.hasMessageInOutbox()) {
			return this.outMessages.remove(0);
		} else {
			throw new IOException("There's no message in the outbox.");
		}		
	}
	
	@SuppressWarnings("unused")
	private void addMessage(Message msg) {
		this.allMessages.add(msg);
	}
	
	/**//* Thread related methods */
	
	
	/**//**//* The start method */
	public void start() {
		new Thread(this).start();
	}
	
	/**//**//* The run method */
	@Override
	public void run() {
		//Update the status of this Runnable
		this.status = ChannelStatusEnum.RUNNING;
		//Start both the Reader and the Writer in threads
		new Thread(this.reader).start();
		new Thread(this.writer).start();
		//Current processed message
		Message currentMsg   = null;
		Flags   currentFlags = null;
		
		//Iterate until the socket is closed
		while (!this.socket.isClosed() && !this.isFinished() && !this.isFinishing()) {
			
			//The thread stays here while paused
			
			while (this.isRunning()) {
				try {
					//Send a message
					if (this.hasMessageInOutbox()) {
						System.out.println("size Message lists : "+this.outMessages.size());
						this.writer.addMessage(this.nextMessage());
					}
					//Receive a message and process it (flag, etc)
					if (this.reader.hasMessage()) {
						currentMsg   = this.reader.nextMessage();
						currentFlags = currentMsg.getFlags();
						//this.addMessage(currentMsg); 	// useless in multicasting
						
						//Flag processing
						if (currentFlags.areActivated()) {
							if (currentFlags.getFlag(FlagEnum.USER_CONNECTION)) {
								if (!currentMsg.getSender().equals(this.appModel.getLocalUser())) {
									if (!currentMsg.getSender().getNickname().equals(this.appModel.getLocalUser().getNickname())) {
										System.out.println("[SERVER]"+this.appModel);
										this.appModel.addUserToList(currentMsg.getSender());
										//this.sendMessage(Message.unicastResponseMessage(this.appModel.getLocalUser(), currentMsg.getSender()));
									}
									this.sendMessage(Message.unicastResponseMessage(this.appModel.getLocalUser(), currentMsg.getSender()));
									
								}
								/*if (!currentMsg.getSender().equals(this.appModel.getLocalUser()) && !currentMsg.getSender().getNickname().equals(this.appModel.getLocalUser().getNickname())) {
									System.out.println("[SERVER]"+this.appModel);
									this.appModel.addUserToList(currentMsg.getSender());
									this.sendMessage(Message.unicastResponseMessage(this.appModel.getLocalUser(), currentMsg.getSender()));
								}*/
							}
							if (currentFlags.getFlag(FlagEnum.USER_CONNECTED)) {
								this.appModel.addUserToList(currentMsg.getSender());
							}
							
							if (currentFlags.getFlag(FlagEnum.USER_WANT_PSEUDO)) {
								if (!currentMsg.getSender().equals(this.appModel.getLocalUser())) {
									System.err.println("Un user demande a changer de pseudo");
									this.sendMessage(Message.unicastResponseMessage(this.appModel.getLocalUser(), currentMsg.getSender()));
								}
							}
							
							if (currentFlags.getFlag(FlagEnum.USER_NEW_PSEUDO)) {
								if (!currentMsg.getSender().equals(this.appModel.getLocalUser())) {
									System.err.println("Un user a change  son pseudo");
									this.appModel.addUserToList(currentMsg.getSender());
								}
							}
							
							if (currentFlags.getFlag(FlagEnum.USER_DISCONNECTION)) {
								System.err.println("un user viens de se deconnecter : "+currentMsg.getSender().getNickname());
								this.appModel.removeUserFromList(currentMsg.getSender());
							}
						}
					}
				} catch (IOException e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
				}
			}
		}
		//End the channel properly
		this.socket.close();
		this.reader.finish();
		this.writer.finish();
		this.end();
	}
}