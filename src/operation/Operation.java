package operation;

public abstract class Operation {
	/* Default Attributes */
	private boolean isSuccessful = false;
	private String info = "";
	
	/* Constructors */
	//Default
	
	/* Abstract Methods */
	//Execute the Operation
	public abstract void execute();
	
	/* Defined Methods */
	public void success() {
		this.isSuccessful = true;
	}
	
	public void fail() {
		this.isSuccessful = false;
	}
	
	public boolean isSuccessful() {
		return this.isSuccessful;
	}
	
	public void setInfo(String s) {
		this.info = s;
	}
	
	public String getInfo() {
		return this.info;
	}
	
	@Override
	public String toString() {
		return this.getInfo();
	}
}
