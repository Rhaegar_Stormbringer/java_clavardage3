package operation;

import com.ClientChannel;

import app.AppModel;
import data.Message;

public class OperationSend extends Operation {
	/* Attribute */
	private Message msg;
	private AppModel appModel;

	/* Constructors */
	public OperationSend(Message msg, AppModel appModel) {
		this.appModel = appModel;
		this.msg = msg;
		this.execute();
	}

	/* Methods */
	@Override
	public void execute() {
		ClientChannel cc = this.appModel.getChannel(this.msg.getReceiver().getIP());
		System.out.println(this.msg.getReceiver());
		System.out.println(cc);
		//Send the message
		if (cc != null) {
			cc.sendMessage(this.msg);
			//Success
			this.success();
			this.setInfo("Your message has been submitted to the corresponding sending channel.");
		} else {
			//Fail
			this.fail();
			this.setInfo("You are not connected to this user!");
		}
	}
}
