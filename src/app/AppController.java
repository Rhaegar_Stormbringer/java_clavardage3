package app;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Observable;
import java.util.Observer;

import data.Message;
import data.User;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import operation.Operation;
import operation.OperationCloseApp;
import operation.OperationConnectTo;
import operation.OperationDisconnectFrom;
import operation.OperationLogin;
import operation.OperationLogout;
import operation.OperationSend;

public abstract class AppController implements Observer{
	/* Attributes */
	/**//* Linkers */
	protected Stage main;
	protected AppModel appModel = null;
	protected AppView  appView = null;
		
	/* Methods */
	/**//* Usual methods */
	public void setAppView(AppView aV) {
		this.appView = aV;
		System.out.println(this.appView);
	}
	
    /**//* Create methods */
    public void createModel() {	
    	//Init :
    	DatagramSocket ipSock;
    	
       	//Creation
    	try {
    		//Retrieve IP :			
			ipSock = new DatagramSocket();
			ipSock.connect(InetAddress.getByName("8.8.8.8"), 0);
			String ip = ipSock.getLocalAddress().getHostAddress();
			
			//Create the model :
			this.appModel = new AppModel(ip, 0, this);//port at 0 => set by system 
			this.appModel.addObserver(this);
	    	//Close the UDP socket used to retrieve the IP address
			ipSock.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}	  	
    }
    
    public void setStage(Stage s) {
    	System.out.println("stage set");
    	this.main = s;
    	this.main.setOnCloseRequest(new EventHandler<WindowEvent>() { @Override
		    public void handle(WindowEvent t) {
    		while(!doCloseApp().getInfo().equals("You may now close the app.")) {}; // wait for clean multicast disconnection
    		System.err.println("disconnection successfull");
		    Platform.exit();
		    System.exit(0);
		    }
		});
    }
    
    public void createView() {
    	this.appView = new AppView();
    }
    
    /**//* Getter */
    //Delete after test
    public AppModel getAppModel() {
    	return this.appModel;
    }

    //Delete after test
    public AppView getAppView() {
    	return this.appView;
    }
    
    
	/**//* Do methods */
	public Operation doLogin(String login, String nickname) {
		return new OperationLogin(login, nickname, this.appModel);		
	}
	
	public Operation doLogout() {
		return new OperationLogout(this.appModel);
	}
	
	public Operation doCloseApp() {
		return new OperationCloseApp(this.appModel);
	}
	
	public Operation doConnectTo(User u) {
		return new OperationConnectTo(u, this.appModel, this);
	}
	
	public Operation doDisconnectFrom(User u) {
		return new OperationDisconnectFrom(u, this.appModel); //TO-DO
	}
	
	public Operation doSend(Message m) {
		return new OperationSend(m, this.appModel); //TO-DO
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
	}
}
