package operation;


import com.ClientChannel;

import app.AppController;
import app.AppModel;
import data.User;

public class OperationConnectTo extends Operation {
	
	/* Attributes */
	private User distantUser;
	private AppModel appModel;
	private AppController appCtrl;
	
	/* Constructors*/
	public OperationConnectTo(User distantUser, AppModel appModel, AppController appCtrl) {
		this.distantUser = distantUser;
		this.appModel = appModel;
		this.appCtrl = appCtrl;
		this.execute();
	}

	/* Override abstract methods */
	@Override
	public void execute() {
		//Create a new channel
		ClientChannel cc;
		
		//Retrieve the old channel :
		ClientChannel oldCC = this.appModel.getChannel(this.distantUser.getIP());
				
		//Try to connect
		try {
			cc = new ClientChannel(this.distantUser.getIP(), this.distantUser.getPort());
			cc.addObserver(this.appCtrl);
		} catch (Exception e) {
			this.fail();
			this.setInfo(e.getMessage());
			cc = null;
		}
		
		if (cc != null) {
			if (oldCC != null) {
				cc.setAllMessageList(oldCC.getAllMessageList());
				oldCC.finish();
				this.appModel.removeChannel(oldCC);
			}
			this.appModel.addChannel(cc);
			Thread t = new Thread(cc);
			t.start();
			this.success();
			this.setInfo("Channel created, you can now chat with" + this.distantUser);
			this.appModel.getUserList().getUserByLogin(this.distantUser.getLogin()).setCommunicationPort(cc.getRemotePort());
		}
		

	}

}
