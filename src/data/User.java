package data;

import java.io.Serializable;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class User implements Serializable, Comparable<User>{ 
	/**
	 * Generated serialVersionUID
	 */
	private static final long serialVersionUID = -2112084647742183541L;
	
	/* Attributes */
	private String         login;
	private String         nickname;
	private String         serverIP;
	private int            serverPort;
	private int            communicationPort = -1;
	private UserStatusEnum status = UserStatusEnum.UNKNOWN;
	
	/* Constructors */
	public User(String login, String nickname, String ip, int port, UserStatusEnum status) {
		this.login = login;
		this.nickname = nickname;
		this.serverIP = ip;
		this.serverPort = port;
		this.status = status;
	}
	
	/* Constructors */
	public User(String login, String nickname, String ip, int port) {
		this.login = login;
		this.nickname = nickname;
		this.serverIP = ip;
		this.serverPort = port;
	}
	
	/* Methods */
	/**//* Usual methods */
	public String getLogin() {
		return this.login;
	}
	
	public String getNickname() {
		return this.nickname;
	}
	
	public String getIP() {
		return this.serverIP;
	}
	
	public int getPort() {
		return this.serverPort;
	}
	
	public int getCommunicationPort() {
		return this.communicationPort;
	}
	
	public UserStatusEnum getStatus() {
		return this.status;
	}
	
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	public String toString() {
		return this.nickname + "@(" + this.serverIP + "-" + this.serverPort+")";
	}
	
	/**//* Status related methods */
	public void connect() {
		this.status = UserStatusEnum.CONNECTED;
	}
	
	public void disconnect() {
		this.status = UserStatusEnum.DISCONNECTED;
	}
	
	public void absent() {
		this.status = UserStatusEnum.ABSENT;
	}
	
	public boolean isConnected() {
		return this.status == UserStatusEnum.CONNECTED;
	}
	
	public boolean isOnline() {
		return (this.status==UserStatusEnum.CONNECTED)||(this.status==UserStatusEnum.ABSENT);
	}
	
	public boolean isOffline() {
		return (this.status==UserStatusEnum.UNKNOWN)||(this.status == UserStatusEnum.DISCONNECTED);
	}
	
	/**//* equals */
	public boolean equals(User u) {
		return (this.login.equalsIgnoreCase(u.getLogin()) && this.nickname.equalsIgnoreCase(u.getNickname()) && this.serverIP.equals(u.getIP()) && this.serverPort == u.getPort() && this.status == u.getStatus());		
	}
	
	/**//* Local user */
	public static User localUser() throws UnknownHostException, SocketException {
		//Retrieve IP address
		DatagramSocket ipSock = new DatagramSocket();
		ipSock.connect(InetAddress.getByName("8.8.8.8"), 0);
		String ip = ipSock.getLocalAddress().getHostAddress();
		ipSock.close();
		
		//Create user
		return new User("","",ip,-1);
	}

	@Override
	public int compareTo(User u) {
		if ((this.serverIP.equals(u.getIP()))
				&&(this.serverPort == u.getPort()
				&&(this.login.equals(u.getLogin()))
				&&(this.nickname.equals(u.getNickname()))	)) {
			return 0;
		} else if (!this.nickname.equals(u.getNickname())){
			return this.nickname.compareTo(u.getNickname());
		} else {
			return -2;
		}
	}

	public void setCommunicationPort(int cP) {
		this.communicationPort = cP;
	}
}
