package com;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import data.*;

public class ClientWriter implements Runnable {
	/* Attributes */
	/**//* Received Messages Buffer */
	private List<Message> msgBuffer =  null;
	private ObjectOutputStream outStream = null;
	private WRStatusEnum status = WRStatusEnum.UNDEFINED;
	
	/* Constructors */
	public ClientWriter (ObjectOutputStream inputStream) {
		this.msgBuffer = Collections.synchronizedList(new ArrayList<Message>());
		this.outStream = inputStream;
		this.status = WRStatusEnum.IDLE;
	}
	
	/* Methods */
	/**//* Buffer related methods */
	public boolean hasMessage() {
		return !this.msgBuffer.isEmpty();
	}
	
	private Message nextMessage() throws IOException {
		if (this.hasMessage()) {
			return this.msgBuffer.remove(0);
		} else {
			throw new IOException("The writer's buffer is empty.");
		}
	}
	
	public void addMessage(Message msg) {
		this.msgBuffer.add(msg);
	}
	
	/**//* Status related methods */
	public boolean isRunning() {
		return (this.status==WRStatusEnum.RUNNING);
	}
	
	public boolean isPaused() {
		return (this.status==WRStatusEnum.PAUSED);
	}
	
	public boolean isFinishing() {
		return (this.status==WRStatusEnum.FINISHING);
	}
	
	public boolean isFinished() {
		return (this.status==WRStatusEnum.FINISHED);
	}
	
	public void pause() {
		this.status = WRStatusEnum.PAUSED; 
	}
	
	public void resume() {
		this.status = WRStatusEnum.RUNNING; 
	}
	
	public void finish() {
		this.status = WRStatusEnum.FINISHING; 
	}
	
	public void end() {
		this.status = WRStatusEnum.FINISHED; 
	}
	
	/**//* Thread related methods */
	/**//**//* The run methods */
	@Override
	public void run() {
		//Update the status of this runnable
		this.status = WRStatusEnum.RUNNING;
		//Iterate until finished
		while (!this.isFinished() && !this.isFinished()) {
			
			//The thread stays here if paused
			
			while (this.isRunning()) {
				try {
					if (this.hasMessage()) {					
						outStream.writeObject(this.nextMessage());
					}
				} catch (IOException e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
				}
			}
		}
		
		//End the channel properly
		try {
			this.outStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.end();
	}
	
	
}
