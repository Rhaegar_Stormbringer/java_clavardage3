package app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;

import com.*;
import data.*;

public class AppModel extends Observable {
	/* Attributes */
	/**//* Clients & Server */
	private List<ClientChannel> channelList;
	private MulticastChannel multiChannel;
	private Server server;
	
	/**//* Users */
	private User localUser;
	private UserList userList;
	
	/**//* Controller */
	private AppController appCtrl;
	
	/* Constructor */
	public AppModel(String login, String nickname, String ip, int port) {
		this.channelList = Collections.synchronizedList(new ArrayList<ClientChannel>());
		this.multiChannel = new MulticastChannel(6666, this);
		this.server = new Server(ip, port, this);
		this.multiChannel.start();
		this.localUser = new User(login, nickname, ip, this.server.getLocalPort(), UserStatusEnum.DISCONNECTED);
		this.userList = new UserList();
		this.server.start();
	}
	
	public AppModel(String ip, int port, AppController appCtrl) {
		this.channelList = Collections.synchronizedList(new ArrayList<ClientChannel>());
		this.multiChannel = new MulticastChannel(6666, this);
		this.server = new Server(ip, port, this);
		this.multiChannel.start();
		this.localUser = new User(null, null, ip, this.server.getLocalPort(), UserStatusEnum.DISCONNECTED);
		this.userList = new UserList();
		this.server.start();
		this.appCtrl = appCtrl;
	}
	
	/* Methods */
	/**//* Usual Methods */
	public AppController getAppController() {
		return this.appCtrl;
	}
	
	public void setAppController(AppController aC) {
		this.appCtrl = aC;
	}
	
	public UserList getUserList() {
		return this.userList;
	}
	
	public void addUserToList(User u) {
		User thisUser = this.userList.getUserByLogin(u.getLogin());
		this.userList.addUser(u);
		if (thisUser != null) {
			this.removeUserFromList(thisUser);
		}
		this.setChanged();
		System.err.println(this.hasChanged());
		this.notifyObservers();
	}
	
	public void updateUserInList(User u) {
		this.addUserToList(u);
	}
	
	public void removeUserFromList(User u) {
		System.out.println("[AppModel] "+u.getNickname()+" | size "+this.userList.getList().size());
		this.userList.removeUser(u);
		System.out.println("[AppModel] size "+this.userList.getList().size());
		this.setChanged();
		this.notifyObservers();
	}
	
	public User getLocalUser() {
		return this.localUser;
	}
	
	public void setLocalUser(User u) {
		this.localUser = u;
	}
	
	
	/**//* Client related methods */
	public ClientChannel addChannel(ClientChannel channel) {
		this.channelList.add(channel);
		return channel;
	} 
	
	public ClientChannel removeChannel(ClientChannel channel) {
		this.channelList.remove(channel);
		return channel;
	}
	
	public ClientChannel getChannel(String remoteIP){
		Iterator<ClientChannel> i = this.channelList.iterator();
		ClientChannel currentChannel = null;
		while (i.hasNext()) {
			currentChannel = i.next();
			if (currentChannel.getRemoteIP().equals(remoteIP)) {
				return currentChannel;
			} 
		}
		return null;		
	}
	
	/**//* Server related methods */
	public Server getServer() {
		return this.server;
	}
	
	/**//* Server related methods */
	public MulticastChannel getMultiChannel() {
		return this.multiChannel;
	}
}
