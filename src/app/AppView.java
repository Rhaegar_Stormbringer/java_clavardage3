package app;

import javax.swing.*;
import java.awt.event.*;

public class AppView extends JFrame{
	private static final long serialVersionUID = 4529398512488667075L;
	
	/* Attributes */
	
	/* Methods */
	public AppView() {
		super("Swing Clavardage");
		
		WindowListener l = new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		};

/////////* LOGIN FIELDS *///////////////////////////////////////////////////////////////////////////////////
		JPanel loginContainer = new JPanel();
		JTextField loginText = new JTextField("Login");
		loginContainer.add(loginText);
		
		
/////////* USER LIST *///////////////////////////////////////////////////////////////////////////////////////
		JPanel userListContainer = new JPanel();
		JLabel userTitleLabel = new JLabel("List of users :");
		userListContainer.add(userTitleLabel);
		
		
		
/////////* AFFICHAGE *///////////////////////////////////////////////////////////////////////////////////////
		getContentPane().add(userListContainer);
		getContentPane().add(loginContainer);
		
		
		addWindowListener(l);
		setSize(200,100);
		setVisible(true);
	}
	
	/* Main Methods */
	public static void main(String[] args) {
		//JFrame frame = new AppView();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	public GuiMain GUI;
	private AppController appController;
	public AppView(AppController appController) {
		this.appController = appController;
		this.appController.setAppView(this);
		System.out.println(this.appController);
		this.GUI = new GuiMain();
		this.GUI.setController(this.appController);
	}
	
	public void start(String [] args) {
		GuiMain.launch(GuiMain.class, args);
	}
	*/
}
