package operation;

import app.AppModel;
import data.Message;
import data.User;
import data.UserStatusEnum;

public class OperationWantPseudo extends Operation {
	/* New Attributes */
	private String login;
	private String nickname;
	private AppModel appModel;
	
	/* Constructors */
	public OperationWantPseudo(String nn, AppModel am) {
		this.nickname = nn;
		this.appModel = am;
		this.execute();
	}
	
	/* Define Abstract Methods */
	@Override
	public void execute() {
		/* Local user information */
		User localUser = this.appModel.getLocalUser();
		User newLocalUser = new User(localUser.getLogin(), this.nickname, localUser.getIP(), localUser.getPort(), UserStatusEnum.CONNECTED);
		//this.appModel.setLocalUser(newLocalUser);
		System.out.println("[JE SUIS BIEN ICI] : "+this.appModel.getLocalUser());
		
		/* Send info to multicast */
		this.appModel.getMultiChannel().sendMessage(Message.multicastWantPseudoMessage(this.appModel.getLocalUser()));
		
		/* Waiting for userList */
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		/* Check userList */
		if (this.appModel.getUserList().getUserByLogin(this.login) != null) {
			//Fail
			this.fail();
			this.setInfo("Ce pseudo est deja pris");
		} else if (this.appModel.getUserList().getUserByNickname(this.nickname) != null) {
			//Fail
			this.fail();
			this.setInfo("Ce pseudo est deja pris");
		} else {
			//Success
			this.appModel.setLocalUser(newLocalUser);
			this.success();
			this.setInfo("success");
		}		
	}
}
