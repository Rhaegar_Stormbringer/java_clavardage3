package data;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class Message implements Serializable, Comparable<Message>{

	/**
	 * Auto-generated serialVersionUID
	 */
	private static final long serialVersionUID = 6837700052666428288L;
	
	/*Attributes*/
	/*/!\ TEST: STRING = USER /!\ SAUF CONTENT */ 
	private User to;
	private User from;
	private String content;
	private Date date;
	private Flags flags;
	private int randID;
	
	/*Constructors*/
	public Message(User from, User to, String content) {
		this.to = to;
		this.from = from;
		this.content = content;
		this.date = new Date();
		this.flags = new Flags();
		this.randID = new Random().nextInt(1001);
	}
	
	/*Methods*/
	
	public int getID() {
		return this.randID;
	}
	
	public User getSender() {
		return this.from;
	}
	
	public User getReceiver() {
		return this.to;
	}
	
	public String getContent() {
		return this.content;
	}
	
	public Date getDate() {
		return this.date;
	}
	
	public Flags getFlags() {
		return this.flags;
	}
	
	@Override
	public String toString() {
		//Convert to calendar
		Calendar cal = Calendar.getInstance();
		cal.setTime(this.date);
		
		//Create output
		String output = "[";
		
		//Get the day
		if (cal.get(Calendar.DAY_OF_MONTH) < 10) {
			output += "0";
		}
		output += cal.get(Calendar.DAY_OF_MONTH)+" ";
		
		//get the month
		switch (cal.get(Calendar.MONTH)) {
			case 0 :
				output += "Jan";
				break;
			case 1 :
				output += "Feb";
				break;
			case 2 :
				output += "Mar";
				break;
			case 3 : 
				output += "Mai";
				break;
			case 4 :
				output += "Apr";
				break;
			case 5 :
				output += "Jun";
				break;
			case 6 :
				output += "Jul";
				break;
			case 7 : 
				output += "Aug";
				break;
			case 8 :
				output += "Sep";
				break;
			case 9 :
				output += "Oct";
				break;
			case 10 :
				output += "Nov";
				break;
			case 11 :
				output += "Dec";		
				break;
		}
		
		//spacer
		output += " ";
		
		//hour
		if (cal.get(Calendar.HOUR_OF_DAY) < 10) {
			output += "0";
		}
		output += cal.get(Calendar.HOUR_OF_DAY)+":";
		
		//minute
		if (cal.get(Calendar.MINUTE)<10) {
			output += "0";
		}
		output += cal.get(Calendar.MINUTE);
		
		//spacer
		output += "] ";
		
		//User nickname
		output += this.from.getNickname()+" : ";
		
		//Content
		output += this.content;
		
		//Output
		return output;				
	}
	
	/**//* Flag related methods */
	public void raiseFlag(FlagEnum flag) {
		this.flags.raiseFlag(flag);
	}
	
	public void lowerFlag(FlagEnum flag) {
		this.flags.lowerFlag(flag);
	}
	
	public boolean getFlag(FlagEnum flag) {
		return this.flags.getFlag(flag);
	}
		
	/**//* Static Methods */
	public static Message systemMessage(String content) {
		return new Message(new User("System", "System", "0.0.0.0", -1), new User("LocalUser", "LocalUser", "0.0.0.0", -1 ), content);
	}
	
	public static Message multicastConnectionMessage(User localUser) {
		Message msg = new Message(localUser, new User("Everyone", "Everyone", "0.0.0.0", -1 ), "");
		msg.raiseFlag(FlagEnum.USER_CONNECTION);
		return msg;
	}
	
	public static Message multicastDisconnectionMessage(User localUser) {
		Message msg = new Message(localUser, new User("Everyone", "Everyone", "0.0.0.0", -1 ), "");
		msg.raiseFlag(FlagEnum.USER_DISCONNECTION);
		return msg;
	}
	
	public static Message multicastResponseMessage(User localUser) {
		Message msg = new Message(localUser, new User("Everyone", "Everyone", "0.0.0.0", -1 ), "");
		msg.raiseFlag(FlagEnum.USER_CONNECTED);
		return msg;
	}
	
	public static Message unicastResponseMessage(User localUser, User sender) {
		Message msg = new Message(localUser, sender, "");
		msg.raiseFlag(FlagEnum.USER_CONNECTED);
		return msg;
	}
	
	public static Message multicastWantPseudoMessage(User localUser) {
		Message msg = new Message(localUser, new User("Everyone", "Everyone", "0.0.0.0", -1 ), "");
		msg.raiseFlag(FlagEnum.USER_WANT_PSEUDO);
		System.out.println("[Message] "+msg.getFlag(FlagEnum.USER_WANT_PSEUDO));
		return msg;
	}
	
	public static Message multicastNewPseudoMessage(User localUser) {
		Message msg = new Message(localUser, new User("Everyone", "Everyone", "0.0.0.0", -1 ), "");
		msg.raiseFlag(FlagEnum.USER_NEW_PSEUDO);
		return msg;
	}
	
	public static Message unicastServerStatusMessage(User localUser, User sender) {
		Message msg = new Message(localUser, sender, "");
		msg.raiseFlag(FlagEnum.SERVER_STATUS);
		return msg;
	}

	@Override
	public int compareTo(Message o) {
		if (this.date.equals(o.getDate())) {
			return 0;
		} else if (this.date.before(o.getDate())) {
			return -1;		
		} else {
			return 1;
		}
	}
	
}
