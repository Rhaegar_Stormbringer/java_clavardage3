package operation;

import app.AppModel;
import data.Message;
import data.User;
import data.UserStatusEnum;

public class OperationLogin extends Operation {
	/* New Attributes */
	private String login;
	private String nickname;
	private AppModel appModel;
	
	/* Constructors */
	public OperationLogin(String lg, String nn, AppModel am) {
		this.login = lg;
		this.nickname = nn;
		this.appModel = am;
		this.execute();
	}
	
	/* Define Abstract Methods */
	@Override
	public void execute() {
		/* Local user information */
		User localUser = this.appModel.getLocalUser();
		User newLocalUser = new User(this.login, this.nickname, localUser.getIP(), localUser.getPort(), UserStatusEnum.CONNECTED);
		this.appModel.setLocalUser(newLocalUser);
		System.out.println("[USER] : "+this.appModel.getLocalUser());
		
		/* Send info to multicast */
		this.appModel.getMultiChannel().sendMessage(Message.multicastConnectionMessage(this.appModel.getLocalUser()));
		
		/* Waiting for userList */
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		/* Check userList */
		if (this.appModel.getUserList().getUserByLogin(this.login) != null) {
			//Fail
			this.fail();
			this.setInfo("Another user is currently using this login.");
		} else if (this.appModel.getUserList().getUserByNickname(this.nickname) != null) {
			//Fail
			this.fail();
			this.setInfo("Another user is currently using this nickname.");
		} else {
			//Success
			this.success();
			this.setInfo("You are logged in");
		}		
	}
}
