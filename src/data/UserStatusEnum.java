package data;

public enum UserStatusEnum {
	UNKNOWN,
	DISCONNECTED,
	ABSENT,
	CONNECTED;
}
