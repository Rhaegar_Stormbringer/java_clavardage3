package com;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;

import data.ChannelStatusEnum;
import app.AppModel;

public class Server implements Runnable {

	/* Attributes */
	/**//* Main attributes */
	private ServerSocket server = null;
	
	/**//* Address attributes */
	private String ip = "127.0.0.1";
	private int port;

	/**//* Status */
	private ChannelStatusEnum status = ChannelStatusEnum.UNDEFINED;

	/**//* Model */
	private AppModel appModel = null;
   
	/* Constructors */
	public Server(AppModel appModel) {
		try {
			server = new ServerSocket(0, 100, InetAddress.getByName(ip));
		}
		catch (UnknownHostException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		this.port = server.getLocalPort();
		this.appModel = appModel;
		this.status = ChannelStatusEnum.IDLE;
	}
   

	public Server(String pHost, int pPort, AppModel appModel) {
		this.ip = pHost;
      	this.port = pPort;   
      	try {
      		server = new ServerSocket(port, 100, InetAddress.getByName(pHost));
      	}
      	catch (UnknownHostException e) {
      		e.printStackTrace();
      	}
      	catch (IOException e) {
      		e.printStackTrace();
      	}
      	this.appModel = appModel;
      	this.status = ChannelStatusEnum.IDLE;
	}
   
	/* Methods */
	/**//* Address related methods */
	public String getLocalIP() {
		return this.server.getInetAddress().getHostAddress();
	}
	
	public int getLocalPort() {
		return this.server.getLocalPort();
	}
	
	/**//* Status related methods */
	public boolean isRunning() {
		return (this.status==ChannelStatusEnum.RUNNING);
	}
	
	public boolean isPaused() {
		return (this.status==ChannelStatusEnum.PAUSED);
	}
	
	public boolean isFinishing() {
		return (this.status==ChannelStatusEnum.FINISHING);
	}
	
	public boolean isFinished() {
		return (this.status==ChannelStatusEnum.FINISHED);
	}
	
	public void pause() {
		this.status = ChannelStatusEnum.PAUSED; 
	}
	
	public void resume() {
		this.status = ChannelStatusEnum.RUNNING; 
	}
	
	public void finish() {
		this.status = ChannelStatusEnum.FINISHING; 
	}
	
	public void end() {
		this.status = ChannelStatusEnum.FINISHED; 
	}
	
	/**//* Thread related methods */
	/**//**//* The start method */
	public void start() {
		new Thread(this).start();
	}
	
	/**//**//* The run method */
	@Override 
	public void run() {
		//Update the status of this Runnable
		this.status = ChannelStatusEnum.RUNNING;
		
		//Iterate until the socket is closed
		while (!this.server.isClosed() && !this.isFinished() && !this.isFinishing()) {
						
			//The thread stays here while paused
			
			while (this.isRunning()) {
				try {
					ClientChannel cc = new ClientChannel(this.server.accept());
					cc.addObserver(this.appModel.getAppController());
					ClientChannel oldCC = this.appModel.getChannel(cc.getRemoteIP());
					if (oldCC != null) {
						cc.setAllMessageList(oldCC.getAllMessageList());
						oldCC.finish();
						this.appModel.removeChannel(oldCC);
					}
					Thread t = new Thread(cc);
					t.start();
					this.appModel.addChannel(cc);
					this.appModel.getUserList().getUserByAddress(cc.getRemoteIP());
				} catch (IOException e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
				}				
			}
		}
		
		//Stop the server
		try {
			this.server.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			this.end();
		}
	}
}