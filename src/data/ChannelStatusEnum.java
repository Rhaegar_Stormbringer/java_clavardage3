package data;

public enum ChannelStatusEnum {
	UNDEFINED,
	IDLE,
	RUNNING,
	PAUSED,
	FINISHING,
	FINISHED;
}
